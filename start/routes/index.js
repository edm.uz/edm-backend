"use strict"

const Route = use('Route');

require('./backend');
require('./frontend');

Route.on('/verify').render('email.verify');
Route.on('*').render('welcome');
