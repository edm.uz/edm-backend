'use strict';

const {test, trait} = use('Test/Suite')('Register');

trait('Test/ApiClient');
trait('DatabaseTransactions');

test('Register user', async ({assert, client}) => {
  const password = '787898aa';
  const response = await client.post('/api/v1/front/register')
    .header('accept', 'application/json')
    .type('json')
    .send({
      username: 'TestUser',
      password,
      password_confirmation: password,
      email: 'test@test.test'
    }).end();

  response.assertStatus(201);
  response.assertJSONSubset({
    data: {
      type: 'bearer',
    }
  });
});
