'use strict'

const {test, trait} = use('Test/Suite')('Login');
const User = use('App/Models/User');

trait('Test/ApiClient');
trait('DatabaseTransactions');

test('Login user', async ({assert, client}) => {
  const username = 'TestUser';
  const email = 'test@test.test';
  const password = '787898aa';

  const user = await User.create({
    username,
    password,
    email
  });

  const response = await client.post('/api/v1/front/login')
    .header('accept', 'application/json')
    .type('json')
    .send({
      email,
      password,
    }).end();

  response.assertStatus(200);
  response.assertJSONSubset({
    data: {
      type: 'bearer',
    }
  });
});
