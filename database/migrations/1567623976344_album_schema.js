'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class AlbumSchema extends Schema {
  up () {
    this.create('albums', (table) => {
      table.increments();
      table.bigInteger('artist_id').index().notNullable();
      table.bigInteger('picture_id').index().notNullable();
      table.string('name', 255).notNullable();
      table.text('description').nullable();
      table.integer('type').nullable();
      table.boolean('published').defaultTo(false);
      table.timestamps()
    })
  }

  down () {
    this.drop('albums')
  }
}

module.exports = AlbumSchema;
