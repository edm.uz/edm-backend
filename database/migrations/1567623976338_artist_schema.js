'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class ArtistSchema extends Schema {
  up () {
    this.create('artists', (table) => {
      table.increments();
      table.bigInteger('user_id').index().nullable();
      table.bigInteger('picture_id').index().nullable();
      table.string('name', 255).notNullable();
      table.text('description').nullable();
      table.string('date').nullable();
      table.string('city', 255).nullable();
      table.string('country', 255).nullable();
      table.timestamps()
    })
  }

  down () {
    this.drop('artists')
  }
}

module.exports = ArtistSchema;
