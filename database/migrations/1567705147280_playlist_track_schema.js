'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');//TODO SUKA

class PlaylistTrackSchema extends Schema {
  up () {
    this.create('playlist_track', (table) => {
      table.increments();
      table.bigInteger('playlist_id').unsigned();
      table.bigInteger('track_id').unsigned();
      // table.bigInteger('user_id').unsigned();
      table.timestamp('added_at').defaultTo(this.fn.now());

      table.foreign('playlist_id').references('id').inTable('playlists').onDelete('CASCADE');
      table.foreign('track_id').references('id').inTable('tracks').onDelete('CASCADE');
      // table.foreign('user_id').references('id').inTable('users').onDelete('CASCADE');
    })
  }

  down () {
    this.drop('playlist_track')
  }
}

module.exports = PlaylistTrackSchema;
