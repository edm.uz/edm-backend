'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class FileSchema extends Schema {
  up () {
    this.create('files', (table) => {
      table.increments();
      table.bigInteger('user_id').index().notNullable();
      table.string('name', 255).notNullable();
      table.string('path', 255).notNullable();
      table.string('extension', 10).notNullable();
      table.timestamps()
    })
  }

  down () {
    this.drop('files')
  }
}

module.exports = FileSchema;
