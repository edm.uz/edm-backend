'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class GenreTrackSchema extends Schema {
  up () {
    this.create('genre_track', (table) => {
      table.increments();
      table.bigInteger('genre_id').unsigned();
      table.bigInteger('track_id').unsigned();

      table.foreign('genre_id').references('id').inTable('genres').onDelete('CASCADE');
      table.foreign('track_id').references('id').inTable('tracks').onDelete('CASCADE');
    })
  }

  down () {
    this.drop('genre_track')
  }
}

module.exports = GenreTrackSchema;
