'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class EventSchema extends Schema {
  up () {
    this.create('events', (table) => {
      table.increments();
      table.string('name').notNullable();
      table.text('description').nullable();
      table.string('address').nullable();
      table.string('address_coords').nullable();
      table.timestamp('date').nullable();
      table.timestamp('date_to').nullable();
      table.timestamps();
    })
  }

  down () {
    this.drop('events')
  }
}

module.exports = EventSchema;
