'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class ArtistUserSchema extends Schema {
  up () {
    this.create('artist_user', (table) => {
      table.increments();
      table.bigInteger('artist_id').unsigned();
      table.bigInteger('user_id').unsigned();

      table.foreign('artist_id').references('id').inTable('artists').onDelete('CASCADE');
      table.foreign('user_id').references('id').inTable('users').onDelete('CASCADE');
    })
  }

  down () {
    this.drop('artist_user')
  }
}

module.exports = ArtistUserSchema;
