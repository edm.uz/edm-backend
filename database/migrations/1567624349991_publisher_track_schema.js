'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class PublisherTrackSchema extends Schema {
  up () {
    this.create('publisher_track', (table) => {
      table.increments();
      table.bigInteger('publisher_id').unsigned();
      table.bigInteger('track_id').unsigned();

      table.foreign('publisher_id').references('id').inTable('publishers').onDelete('CASCADE');
      table.foreign('track_id').references('id').inTable('tracks').onDelete('CASCADE');
    })
  }

  down () {
    this.drop('publisher_track');
  }
}

module.exports = PublisherTrackSchema;
