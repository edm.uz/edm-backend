'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class PlaylistSchema extends Schema {
  up () {
    this.create('playlists', (table) => {
      table.increments();
      table.bigInteger('user_id').index().notNullable();
      table.bigInteger('picture_id').index().nullable();
      table.string('name', 255).notNullable();
      table.text('description').nullable();
      table.timestamps()
    })
  }

  down () {
    this.drop('playlists')
  }
}

module.exports = PlaylistSchema;
