'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class ArtistTrackSchema extends Schema {
  up () {
    this.create('artist_track', (table) => {
      table.increments();
      table.bigInteger('artist_id').unsigned();
      table.bigInteger('track_id').unsigned();

      table.foreign('artist_id').references('id').inTable('artists').onDelete('CASCADE');
      table.foreign('track_id').references('id').inTable('tracks').onDelete('CASCADE');
    })
  }

  down () {
    this.drop('artist_track');
  }
}

module.exports = ArtistTrackSchema;
