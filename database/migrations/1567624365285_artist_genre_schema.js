'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class ArtistGenreSchema extends Schema {
  up () {
    this.create('artist_genre', (table) => {
      table.increments();
      table.bigInteger('artist_id').unsigned();
      table.bigInteger('genre_id').unsigned();

      table.foreign('artist_id').references('id').inTable('artists').onDelete('CASCADE');
      table.foreign('genre_id').references('id').inTable('genres').onDelete('CASCADE');
    })
  }

  down () {
    this.drop('artist_genre')
  }
}

module.exports = ArtistGenreSchema;
