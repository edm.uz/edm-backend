'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class ArtistEventSchema extends Schema {
  up () {
    this.create('artist_event', (table) => {
      table.increments();
      table.bigInteger('artist_id').unsigned();
      table.bigInteger('event_id').unsigned();

      table.foreign('artist_id').references('id').inTable('artists').onDelete('CASCADE');
      table.foreign('event_id').references('id').inTable('events').onDelete('CASCADE');
    })
  }

  down () {
    this.drop('artist_event')
  }
}

module.exports = ArtistEventSchema;
