'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class PublisherSchema extends Schema {
  up () {
    this.create('publishers', (table) => {
      table.increments();
      table.bigInteger('picture_id').index().nullable();
      table.bigInteger('publisher_id').index().nullable();
      table.string('name', 255).notNullable();
      table.string('owner', 255).nullable();
      table.string('founder', 255).nullable();
      table.text('description').nullable();
      table.string('date').nullable();
      table.string('country', 255).nullable();
      table.timestamps()
    })
  }

  down () {
    this.drop('publishers')
  }
}

module.exports = PublisherSchema;
