'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class GenrePublisherSchema extends Schema {
  up () {
    this.create('genre_publisher', (table) => {
      table.increments();
      table.bigInteger('genre_id').unsigned();
      table.bigInteger('publisher_id').unsigned();

      table.foreign('genre_id').references('id').inTable('genres').onDelete('CASCADE');
      table.foreign('publisher_id').references('id').inTable('publishers').onDelete('CASCADE');
    })
  }

  down () {
    this.drop('genre_publisher')
  }
}

module.exports = GenrePublisherSchema;
