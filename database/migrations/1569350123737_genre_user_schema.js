'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class GenreUserSchema extends Schema {
  up () {
    this.create('genre_user', (table) => {
      table.increments();
      table.bigInteger('genre_id').unsigned();
      table.bigInteger('user_id').unsigned();

      table.foreign('genre_id').references('id').inTable('genres').onDelete('CASCADE');
      table.foreign('user_id').references('id').inTable('users').onDelete('CASCADE');
    })
  }

  down () {
    this.drop('genre_user');
  }
}

module.exports = GenreUserSchema;
