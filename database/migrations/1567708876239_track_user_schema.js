'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class TrackUserSchema extends Schema {
  up () {
    this.create('track_user', (table) => {
      table.increments();
      table.bigInteger('track_id').unsigned();
      table.bigInteger('user_id').unsigned();
      table.timestamp('added_at').defaultTo(this.fn.now());

      table.foreign('track_id').references('id').inTable('tracks').onDelete('CASCADE');
      table.foreign('user_id').references('id').inTable('users').onDelete('CASCADE');
    })
  }

  down () {
    this.drop('track_user')
  }
}

module.exports = TrackUserSchema;
