'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class GenreSchema extends Schema {
  up () {
    this.create('genres', (table) => {
      table.increments();
      table.bigInteger('picture_id').index().nullable();
      table.string('name', 255).notNullable();
      table.text('description').nullable();
      table.string('date').nullable();
      table.timestamps()
    })
  }

  down () {
    this.drop('genres')
  }
}

module.exports = GenreSchema;
