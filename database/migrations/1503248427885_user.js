'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class UserSchema extends Schema {
  up () {
    this.create('users', (table) => {
      table.increments();
      table.bigInteger('picture_id').index().notNullable();//
      table.string('username', 80).index().notNullable().unique();//
      table.string('full_name', 60).nullable();//
      table.string('email', 254).index().notNullable().unique();//
      table.string('password', 60).notNullable();//
      table.timestamp('date').nullable();//
      table.string('account_status');
      table.text('description').nullable();//
      table.timestamp('banned_at').nullable();//
      table.timestamps()
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema;
