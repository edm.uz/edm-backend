'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class ArtistPublisherSchema extends Schema {
  up () {
    this.create('artist_publisher', (table) => {
      table.increments();
      table.bigInteger('artist_id').unsigned();
      table.bigInteger('publisher_id').unsigned();

      table.foreign('artist_id').references('id').inTable('artists').onDelete('CASCADE');
      table.foreign('publisher_id').references('id').inTable('publishers').onDelete('CASCADE');
    })
  }

  down () {
    this.drop('artist_publisher')
  }
}

module.exports = ArtistPublisherSchema;
