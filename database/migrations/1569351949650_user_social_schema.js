'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class UserSocialSchema extends Schema {
  up () {
    this.create('user_socials', (table) => {
      table.increments();
      table.bigInteger('user_id').unsigned();
      table.text('link').notNullable();
      table.string('type', 200).nullable();
      table.timestamps();

      table.foreign('user_id').references('id').inTable('users').onDelete('CASCADE');
    })
  }

  down () {
    this.drop('user_socials');
  }
}

module.exports = UserSocialSchema;
