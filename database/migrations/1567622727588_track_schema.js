'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class TrackSchema extends Schema {
  up() {
    this.create('tracks', (table) => {
      table.increments();
      table.bigInteger('user_id').index().notNullable();
      table.bigInteger('file_id').index().notNullable();
      table.bigInteger('picture_id').index().notNullable();
      table.bigInteger('album_id').index().nullable();
      table.string('artists').index().notNullable();
      table.string('name').index().notNullable();
      table.integer('bpm').nullable();
      table.text('text').nullable();
      table.text('description').nullable();
      table.boolean('published').defaultTo(false);
      table.timestamps()
    })
  }

  down() {
    this.drop('tracks')
  }
}

module.exports = TrackSchema;
