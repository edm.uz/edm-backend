'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class AlbumArtistSchema extends Schema {
  up () {
    this.create('album_artist', (table) => {
      table.increments();
      table.bigInteger('album_id').unsigned();
      table.bigInteger('artist_id').unsigned();

      table.foreign('album_id').references('id').inTable('albums').onDelete('CASCADE');
      table.foreign('artist_id').references('id').inTable('artists').onDelete('CASCADE');
    })
  }

  down () {
    this.drop('album_artist')
  }
}

module.exports = AlbumArtistSchema;
