'use strict';

const Validator = require('../Validator');

class FrontendAuthRegisterRequest {
  get rules () {
    return {
      username: 'required|alpha',
      email: 'required|email',
      password: 'required|confirmed',
    }
  }

  get messages() {
    return {
      'username.required': 'Логин обязателен для заполнения',
      'username.alpha': 'В логине должны присутствовать только латинские символы',
      'email.required': 'E-mail обязателен для заполнения',
      'password.required': 'Пароль обязателен для заполнения',
      'password.confirmed': 'Пароли не совпадают или вы не ввели повторно пароль',
    }
  }
}

module.exports = FrontendAuthRegisterRequest;
