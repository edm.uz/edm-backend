'use strict';

const Validator = require('../Validator');

class FrontendAuthLoginRequest extends Validator {
  get rules() {
    return {
      email: 'required',
      password: 'required',
    }
  }

  get messages() {
    return {
      'email.required': 'E-mail обязателен для заполнения',
      'password.required': 'Пароль обязателен для заполнения',
    }
  }
}

module.exports = FrontendAuthLoginRequest;
