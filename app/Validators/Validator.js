"use strict";

class Validator {
  async fails(errorMessages) {
    return this.ctx.response.validate(errorMessages)
  }

  get validateAll() {
    return true
  }
}

module.exports = Validator;
