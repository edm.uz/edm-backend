'use strict';

const Persona = use('Persona');
const User = use('App/Models/User');

class AuthController {
  async login({request, auth, response}) {
    let payload = request.only(['email', 'username', 'password']);
    payload.uid = (payload['username'] ? payload['username'] : payload['email']);
    try {
      const user = await Persona.verify(payload);
      const token = await auth.generate(user);
      response.data(token, 'Вы успешно вошли в систему');
    } catch (e) {
      response.data([], 'Неверный e-mail или пароль', 401);
    }
  }

  async register({request, response, auth}) {
    const payload = request.only(['username', 'email', 'password', 'password_confirmation']);
    const {email, username} = payload;
    let user = await User
      .query()
      .where('email', email)
      .orWhere('username', username)
      .first();

    if (user) {
      return response.data([], 'Данный e-mail или логин зарегистрирован', 401);
    }

    user = await Persona.register(payload);

    if (user) {
      const token = await auth.generate(user);
      return response.data(token, 'Вы успешно зарегистрировались!', 201);
    }
    return response.data([], 'Пользователь не создан', 400);
  }
}

module.exports = AuthController;
