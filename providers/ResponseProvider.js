'use strict';

const { ServiceProvider } = require('@adonisjs/fold');

const Response = require('@adonisjs/framework/src/Response');

class ResponseProvider extends ServiceProvider {
  /**
   * Register namespaces to the IoC container
   *
   * @method register
   *
   * @return {void}
   */
  register () {
    //
  }

  /**
   * Attach context getter when all providers have
   * been registered
   *
   * @method boot
   *
   * @return {void}
   */
  boot () {
    Response.macro('validate', function (errors) {
      this.status(422).json({
        message: 'Ошибка валидации',
        errors,
        data: [],
      });
    });

    Response.macro('data', function (data = [], message = 'Успешно', code = 200) {
      this.status(code).json({
        message,
        errors: [],
        data,
      });
    });
  }
}

module.exports = ResponseProvider;
