'use strict'

const {ServiceProvider} = require('@adonisjs/fold')

class ValidatorProvider extends ServiceProvider {
  /**
   * Register namespaces to the IoC container
   *
   * @method register
   *
   * @return {void}
   */
  register() {
    this.app.manager('@adonisjs/validator/providers/ValidatorProvider', {
      extend() {

      }
    })
  }

  /**
   * Attach context getter when all providers have
   * been registered
   *
   * @method boot
   *
   * @return {void}
   */
  boot() {
    // const Validator = this.app.use('Validator');
    // Validator.global('fails', function (errorMessages) {
    //   return this.ctx.response.validate(errorMessages);
    // });
    //
    // Validator.global('validateAll', function () {
    //   return true;
    // });
  }
}

module.exports = ValidatorProvider
